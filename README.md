<h1 align="center">OTG Server Global - Premium Version</h1>
<h2>Tools and Clients</h2>
<p>Download SDK (Software Development Kit) for OTX Server [DOWNLOAD HERE](https://gitlab.com/guilhermesidney/cliente10/-/archive/master/cliente10-master.zip)</p>
<h2>Steps of Compilation</h2>
<p>Our system can be compiled on a variety of operating systems. We currently provide a wiki with build instructions for the following systems:</p>
<table>
<tbody>
<tr>
<td><a href="https://github.com/mattyx14/otxserver/wiki/Compilling-OTX3.2--on-Linux">Debian GNU</a></td>
<td><a href="https://github.com/mattyx14/otxserver/wiki/Compilling-OTX3.2--on-Linux">Ubuntu</a></td>
<td><a href="https://github.com/mattyx14/otxserver/wiki/Compilling-on-Windows">Windows</a></td>
</tr>
</tbody>
</table>
<h2>Websites</h2>
<p>&nbsp;[Marco Oliveira](http://github.com/marcomoa) -&gt; [Download here](https://github.com/marcomoa/Gesior-AAC/archive/master.zip) 
<p>by &nbsp;[Slawkens](http://github.com/slawkens) -&gt; [Download here](https://codeload.github.com/slawkens/myaac/zip/master)</p>
<h2>Contact</h2>
<p>[Whatsapp Invite](<a href="https://chat.whatsapp.com/ELM9aLYegRC0dMid7Xa5hH">https://chat.whatsapp.com/JtKmezKYrw5DEK5Zl9qvPw</a>)&nbsp;</p>
<p>&nbsp;</p>